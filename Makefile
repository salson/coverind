CXX=g++
CXXFLAGS=-O2 -Wall -g -DNDEBUG
SRC=$(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)
EXE=$(SRC:.cpp=)
LDFLAGS=-lsdsl -lz -lhts

all: $(EXE)

$(EXE): %: %.o lib/gzstream.o
	$(CXX) -o $@ $^  $(LDFLAGS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $^ -o $@

clean:
	rm -f $(OBJ) $(EXE)

.PHONY: all clean
