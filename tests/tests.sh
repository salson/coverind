#!/bin/bash
set -e

INDEX=$(tempfile)
../coverind index -b $INDEX data/test1.bam

RESULTS=$(tempfile)
../coverind query -l -i $INDEX > $RESULTS
diff $RESULTS data/expected_list

(
    ../coverind query -i $INDEX -r 1:2-5
    ../coverind query -i $INDEX -r 1:0-0
    ../coverind query -i $INDEX -r 1:9-9
    ../coverind query -i $INDEX -r A:0-0
    ../coverind query -i $INDEX -r A:8-10
) > $RESULTS
diff $RESULTS data/expected_range

../coverind query -i $INDEX -b $RESULTS
diff <(zcat $RESULTS) data/expected_whole_bed

../coverind query -i $INDEX -b $RESULTS -w 1 -r A:5-9
diff <(zcat $RESULTS) data/expected_range_bed

rm -f $RESULTS $INDEX*

