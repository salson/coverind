#include <multicover.hpp>
#include <sdsl/rrr_vector.hpp>
#include <iostream>
#include <cassert>
#include <tuple>
#include <lib/gzstream.h>
#include <cstdio>

using namespace sdsl;

void check_bed_output(const std::string filename,
                      std::tuple<std::string, int, int, int> lines[],
                      size_t nb_lines) {
  igzstream input(filename.c_str());
  std::string chr;
  int start, end, nb_reads;
  for (size_t i = 0; i < nb_lines; i++) {
    input >> chr >> start >> end >> nb_reads;
    assert(chr == std::get<0>(lines[i]));
    assert(start == std::get<1>(lines[i]));
    assert(end == std::get<2>(lines[i]));
    assert(nb_reads == std::get<3>(lines[i]));
  }
  input >> chr;
  assert(input.eof());
}

int main() {
  MultiCover<rrr_vector<> >cover("data/test1.bam");

  assert(cover.getNbChromosomes() == 2);
  assert(cover.getChrName(0) == "1");
  assert(cover.getChrName(1) == "A");
  assert(cover.getChrIndex("1") == 0);
  assert(cover.getChrIndex("A") == 1);

  std::string bed2 = std::tmpnam(nullptr);
  std::string bed1 = std::tmpnam(nullptr);
  
  cover.generateBED(2, bed2);
  cover.generateBED(1, bed1);

  std::tuple<std::string, int, int, int> lines2[]
    = {{"1", 0, 2, 4},
       {"1", 2, 4, 4},
       {"1", 4, 6, 3},
       {"1", 6, 8, 2 },
       {"1", 8, 10, 1},
       {"A", 0, 2, 0},
       {"A", 2, 4, 2},
       {"A", 4, 6, 2},
       {"A", 6, 8, 2},
       {"A", 8, 10, 2}};
  size_t nb_lines2 = sizeof(lines2) / sizeof(std::tuple<std::string, int, int, int>);
  check_bed_output(bed2, lines2, nb_lines2);
  std::remove(bed2.c_str());

  std::tuple<std::string, int, int, int> lines1[]
    = {{"1", 0, 1, 1},
       {"1", 1, 2, 4},
       {"1", 2, 3, 4},
       {"1", 3, 4, 3},
       {"1", 4, 5, 1},
       {"1", 5, 6, 2},
       {"1", 6, 7, 2},
       {"1", 7, 8, 2},
       {"1", 8, 9, 1},
       {"1", 9, 10, 0},
       {"A", 0, 1, 0},
       {"A", 1, 2, 0},
       {"A", 2, 3, 2},
       {"A", 3, 4, 2},
       {"A", 4, 5, 2},
       {"A", 5, 6, 1},
       {"A", 6, 7, 2},
       {"A", 7, 8, 2},
       {"A", 8, 9, 0},
       {"A", 9, 10, 2},
       {"A", 10, 11, 2}};
  size_t nb_lines1 = sizeof(lines1) / sizeof(std::tuple<std::string, int, int, int>);

  check_bed_output(bed1, lines1, nb_lines1);
  std::remove(bed1.c_str());

  // Saving index
  std::string index_name = tmpnam(nullptr);
  cover.save(index_name);
  // Loading
  MultiCover<rrr_vector<> > cover2(index_name, true);
  std::string bed1_bis = std::tmpnam(nullptr);
  std::string bed2_bis = std::tmpnam(nullptr);
  cover2.generateBED(1, bed1_bis);
  cover2.generateBED(2, bed2_bis);
  check_bed_output(bed1_bis, lines1, nb_lines1);
  check_bed_output(bed2_bis, lines2, nb_lines2);
  std::remove(bed1_bis.c_str());
  std::remove(bed2_bis.c_str());

  std::string cmd = "rm -f "+index_name+"*";
  std::system(cmd.c_str());
}
