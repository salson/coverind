#include <coverind.hpp>
#include <sdsl/sd_vector.hpp>
#include <iostream>
#include <cassert>
#include <tuple>

using namespace sdsl;



int main() {
  // Example data :
  // read1: 0-6
  // read2: 1-7
  // read3: 1-9
  // read4: 0-2
  // read5: 3-8
  // Début
  //   0  12 3456789
  // 110110010000000
  // Fin
  // 01 2345 6 7 8 9
  // 001000010101010
  Coverind<sd_vector<> >cover = Coverind<sd_vector<> >("data/start1", "data/end1");
  sd_vector<> bv = cover.getStartBV();
  sd_vector<> end = cover.getEndBV();
  size_t start_bits[] = {1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
  size_t end_bits[] = {0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0};
  std::cout << cover.getStartBV() << std::endl;
  std::cout << cover.getEndBV() << std::endl;
  for (size_t i = 0; i < bv.size(); i++) {
    assert(bv[i] == start_bits[i]);
    assert(end[i] == end_bits[i]);
  }
  //                          0  1  2  3  4  5  6  7  8  9
  size_t nb_reads_before[] = {0, 0, 0, 1, 1, 1, 1, 2, 3, 4}; // Reads ending before position
  size_t nb_reads_overlap[] = {2, 4, 4, 4, 4, 4, 4, 3, 2, 1}; // Reads overlapping position
  std::tuple<size_t, size_t, size_t> nb_reads_between[]
    = {std::make_tuple(0, 2, 4),
       std::make_tuple(0, 3, 5),
       std::make_tuple(1, 2, 4),
       std::make_tuple(1, 3, 5),
       std::make_tuple(6, 9, 4),
       std::make_tuple(7, 9, 3),
       std::make_tuple(8, 9, 2),
       std::make_tuple(9, 9, 1) };
                                                    

  for (size_t i = 0; i < 10; i++) {
    assert(cover.count_reads_end_before(i) == nb_reads_before[i]);
    assert(cover.count_reads_overlapping(i) == nb_reads_overlap[i]);
  }

  for (size_t i = 0; i < 8; i++) {
    size_t count = cover.count_reads_between(std::get<0>(nb_reads_between[i]),
                                             std::get<1>(nb_reads_between[i]));
      PRINT_VAR(count);
    assert(count == std::get<2>(nb_reads_between[i]));
  }

  assert(cover.nb_genome_positions() == 11); // Should be 10 but we need to
                                             // store 16 bits
}
