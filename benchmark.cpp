#include "multicover.hpp"
#include <iostream>
#include <fstream>
#include <chrono>
#include <sdsl/sd_vector.hpp>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <sdsl/rrr_vector.hpp>
#include "lib/gzstream.h"
using namespace sdsl;

const int NB_QUERIES = 100000;
const int RANGE_LENGTH = 100000;
const int WINDOW_LENGTH = 1000;

// template <class bv>
// float launch_random_queries(MultiCover<bv> &cover) {
//   size_t queries[NB_QUERIES];
//   size_t lengths[NB_QUERIES];
//   for (size_t i = 0; i < NB_QUERIES; i++) {
//     size_t chr = rand() % cover.getNbChromosomes();
//     size_t nb_pos = cover.getChrCoverage(chr).nb_genome_positions();
//     size_t range_length = RANGE_LENGTH;
//     if (RANGE_LENGTH > nb_pos) {
//       range_length = nb_pos / 10;
//     }
//     queries[i] = rand() % (nb_pos - range_length);
//     lengths[i] = min(range_length, nb_pos - queries[i]);
//   }
  
//   std::cout << "Running queries..." << std::endl;
//   auto query_start = std::chrono::system_clock::now();
//   size_t total = 0;
//   for (size_t i = 0; i < NB_QUERIES; i++) {
//     size_t count = cover.count_reads_between(queries[i], queries[i] + lengths[i] - 1);
//     total += count;
//     //    std::cerr << queries[i] << "\t" << queries[i] + RANGE_LENGTH << "\t" << count << std::endl;
//   }
//   std::cout << total << std::endl;
//   auto query_end = std::chrono::system_clock::now();

//   return std::chrono::duration_cast<std::chrono::microseconds>(query_end - query_start).count()*1./NB_QUERIES;
// }

template <class bv>
float launch_real_queries(MultiCover<bv> &cover,
                          const std::string query_filename) {
  std::ifstream query_file(query_filename);
  size_t nb_lines = 0;
  if (! query_file.is_open()) {
    throw;
  }
  std::string line;
  while (!query_file.eof()) {
    getline(query_file, line);
    nb_lines++;
  }

  query_file = std::ifstream(query_filename);
  
  std::tuple<size_t, size_t, size_t> queries[nb_lines];
  size_t expected_counts[nb_lines];
  size_t i = 0;
  std::string chr, start_str, end_str, count_str;
  size_t start, end, count;
  while (!query_file.eof()) {
    //    query_file >> chr >> start >> end >> count;
    getline(query_file, chr, '\t');
    if (query_file.eof())
      break;
    getline(query_file, start_str, '\t');
    getline(query_file, end_str, '\t');
    getline(query_file, count_str);
    start = std::stoi(start_str);
    end = std::stoi(end_str);
    count = std::stoi(count_str);
    queries[i] = std::make_tuple(cover.getChrIndex(chr), start, end);
    expected_counts[i] = count;
    i++;
  }
  query_file.close();

  auto query_start = std::chrono::system_clock::now();
  for (i = 0; i < nb_lines; i++) {
    size_t count = cover.getChrCoverage(std::get<0>(queries[i])).count_reads_between(std::get<1>(queries[i]), std::get<2>(queries[i]) - 1);
    std::cout << std::get<0>(queries[i]) << "\t"
              << std::get<1>(queries[i]) << "\t"
              << std::get<2>(queries[i]) << "\t" << count
              << "\t" << expected_counts[i] << std::endl;
  }
  auto query_end = std::chrono::system_clock::now();
  return std::chrono::duration_cast<std::chrono::microseconds>(query_end - query_start).count()*1./nb_lines;
}

template <class bv>
float launch_window_queries(MultiCover<bv> &cover, const std::string bed_filename) {
  auto query_start = std::chrono::system_clock::now();
  cover.generateBED(WINDOW_LENGTH, bed_filename);
  auto query_end = std::chrono::system_clock::now();
  return std::chrono::duration_cast<std::chrono::milliseconds>(query_end - query_start).count()*1.;
}

template <class bv>
void launch_benchmark(char *bam_file, const std::string query_filename="") {
  std::cout << "For file " << bam_file << std::endl;
  auto build_start = std::chrono::system_clock::now();
  MultiCover<bv> cover = MultiCover<bv>(bam_file);
  auto build_end = std::chrono::system_clock::now();
  bool has_real_queries = (query_filename.size() > 0);
  float query_time;
  if (has_real_queries) {
    query_time = launch_real_queries<bv>(cover, query_filename);
  } // else {
  //   query_time = launch_random_queries<bv>(cover);
  // }
   float window_time = launch_window_queries<bv>(cover, std::string(bam_file) + ".anwsers.gz");
  auto save_start = std::chrono::system_clock::now();
  cover.save(bam_file);
  auto save_stop = std::chrono::system_clock::now();

  auto load_start = std::chrono::system_clock::now();
  MultiCover<bv> cover2(bam_file, true);
  auto load_stop = std::chrono::system_clock::now();
  assert (cover.nb_genome_positions() == cover2.nb_genome_positions());
  assert (cover.count_reads_between(100, cover.nb_genome_positions() / 10) == cover2.count_reads_between(100, cover.nb_genome_positions() / 10));
  
  std::cout << "Build time (ms)\t" << std::chrono::duration_cast<std::chrono::milliseconds>(build_end - build_start).count() << std::endl;
  std::cout << "Query time (ns per query)\t" <<  query_time << std::endl;
  std::cout << "Traversing the genome (ms)\t" << window_time << std::endl;
  std::cout << "Space (MB)\t" << cover.size_in_bytes()*1. / 1000000 << std::endl;
  std::cout << "Saving (ms)\t" << std::chrono::duration_cast<std::chrono::milliseconds>(save_stop - save_start).count() << std::endl;
  std::cout << "Loading (ms)\t" << std::chrono::duration_cast<std::chrono::milliseconds>(load_stop - load_start).count() << std::endl;

}

int main(int argc, char ** argv) {
  if (argc <= 1) {
    std::cerr << "Usage: " << argv[0] << " [-q file] bam_file" << std::endl;
    exit(1);
  }

  std::string query_filename = "";
  int index_start = 1;
  if (std::string(argv[1]) == std::string("-q")) {
    query_filename = argv[2];
    index_start += 2;
  }
  
  auto seed = time(NULL);
  std::cout << "*** SD vector" << std::endl;
  srand(seed);
  launch_benchmark<sd_vector<> >(argv[index_start] , query_filename);
  std::cout << "*** RRR vector" << std::endl;
  srand(seed);
  launch_benchmark<rrr_vector<> >(argv[index_start], query_filename);
  
}
