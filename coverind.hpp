#ifndef COVERIND_HPP
#define COVERIND_HPP

#include <iostream>
#include <string>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/sd_vector.hpp>
#include <sdsl/select_support.hpp>

#define PRINT_VAR(v) std::cerr << #v << " = " << v << std::endl

using namespace sdsl;

template <class bv> class Coverind {

private:
  bv start, end;
  typename bv::rank_1_type rank_start, rank_end;
  typename bv::select_0_type select_start, select_end;
public:

  /**
   * Builds from two files containing the bit vectors
   */
  Coverind(const std::string start_file, const std::string end_file);

  /**
   * Builds from the two bit vectors
   */
  Coverind(const bit_vector &start_bv, const bit_vector &stop_bv);
  
  /**
   * Build from an existing index
   */
  Coverind(const std::string index);
  
  bv getStartBV() const;
  bv getEndBV() const;
  
  /**
   * Count all the reads that end before a position
   */
  size_t count_reads_end_before(size_t position) const;
  /**
   * Count all the reads that start before a position
   */ 
  size_t count_reads_start_before(size_t position) const;
  /**
   * Count all the reads that overlap a position
   * (ie. they cover the position)
   */
  size_t count_reads_overlapping(size_t position) const;

  /**
   * Count all the reads that cover a position between start and end
   * (both included)
   */
  size_t count_reads_between(size_t start, size_t end) const;

  /**
   * Return the number of genomic positions (ie. the number of 0s)
   */
  size_t nb_genome_positions() const;

  /**
   * Size of the structure in bytes
   */
  size_t size_in_bytes() const;
  
  void save(const std::string) const;

private:
  void create_rank_select();
  void build_from_bitvectors(const bit_vector &start_bv, const bit_vector &end_bv);
};

bit_vector create_bitvector_from_file(const std::string filename);


//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

template <class bv>
Coverind<bv>::Coverind(const std::string start_file, const std::string end_file) {
  bit_vector start_bv = create_bitvector_from_file(start_file);
  bit_vector end_bv = create_bitvector_from_file(end_file);
  build_from_bitvectors(start_bv, end_bv);
}

template <class bv>
Coverind<bv>::Coverind(const bit_vector &start_bv, const bit_vector &end_bv) {
  build_from_bitvectors(start_bv, end_bv);
}

template<class bv>
void Coverind<bv>::build_from_bitvectors(const bit_vector &start_bv, const bit_vector &end_bv) {
  start = bv(start_bv);
  end = bv(end_bv);

  create_rank_select();
}

template <class bv>
void Coverind<bv>::create_rank_select() {
  rank_start = typename bv::rank_1_type(&start);
  rank_end = typename bv::rank_1_type(&end);

  select_start = typename bv::select_0_type(&start);
  select_end = typename bv::select_0_type(&end);
}

template <class bv>
Coverind<bv>::Coverind(const std::string index) {
  load_from_file(start, index+std::string(".start.bv"));
  load_from_file(end, index +std::string(".end.bv"));

  create_rank_select();
}
                       
template <class bv>
bv Coverind<bv>::getStartBV() const {
  return start;
}

template <class bv>
bv Coverind<bv>::getEndBV() const {
  return end;
}

template <class bv>
size_t Coverind<bv>::count_reads_end_before(size_t position) const {
  if (position == 0)
    return 0;
  size_t bv_pos = select_end(position);
  return rank_end(bv_pos);
}

template <class bv>
size_t Coverind<bv>::count_reads_start_before(size_t position) const {
  if (position == 0)
    return 0;
  size_t bv_pos = select_start(position);
  return rank_start(bv_pos);
}

template <class bv>
size_t Coverind<bv>::count_reads_overlapping(size_t position) const {
  size_t bv_pos = select_start(position + 1);
  return rank_start(bv_pos) - count_reads_end_before(position);
}

template <class bv>
size_t Coverind<bv>::count_reads_between(size_t start, size_t end) const {
  size_t nb_reads_until_end = count_reads_start_before(end + 1);
  size_t nb_reads_before_start = count_reads_end_before(start);
  return nb_reads_until_end - nb_reads_before_start;
}

template <class bv>
size_t Coverind<bv>::nb_genome_positions() const {
  return start.size() - rank_start(start.size());
}

template <class bv>
size_t Coverind<bv>::size_in_bytes() const  {
  return ::size_in_bytes(start) + ::size_in_bytes(end) +
    ::size_in_bytes(rank_start) + ::size_in_bytes(rank_end) +
    ::size_in_bytes(select_start) + ::size_in_bytes(select_end);
}

template <class bv>
void Coverind<bv>::save(const std::string filename) const {
  store_to_file(start, filename + std::string(".start.bv"));
  store_to_file(end, filename + std::string(".end.bv"));
}

bit_vector create_bitvector_from_file(const std::string filename) {
  std::ifstream input(filename);
  input.seekg(0, input.end);
  size_t length = 8*input.tellg();
  input.seekg(0, input.beg);
  bit_vector bv(length, 0);
  char c;
  size_t pos = 0;
  while (! input.eof()) {
    c = input.get();
    if (input.eof())
      break;
    int mask = 1<<7;
    while (mask > 0) {
      if (c & mask)
        bv[pos] = 1;
      pos++;
      mask >>= 1;
    }
  }
  return bv;
}

#endif
